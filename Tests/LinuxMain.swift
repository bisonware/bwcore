import XCTest

import BWCoreTests

var tests = [XCTestCaseEntry]()
tests += BWCoreTests.allTests()
XCTMain(tests)
