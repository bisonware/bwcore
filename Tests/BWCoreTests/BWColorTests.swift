import XCTest
@testable import BWCore

final class BWColorTests: XCTestCase {
    func testColorRedEqual() {
        let color = BWCore.Color.fromHexString("FF0000")
        XCTAssertEqual(color, .red)
    }
    
    func testColorsAreEqual() {
        let color = BWCore.Color.fromHexString("006736")
        XCTAssertEqual(color, BWCore.Color.bwColor)
    }
    
    func testSecondaryColorsAreEqual() {
        let color = BWCore.Color.fromHexString("FCFFFD")
        XCTAssertEqual(color, BWCore.Color.secondaryColor)
    }
    
    static var allTests = [
        ("testColorRedEqual", testColorRedEqual),
        ("testColorsAreEqual", testColorsAreEqual),
        ("testSecondaryColorsAreEqual", testSecondaryColorsAreEqual)
    ]
}
