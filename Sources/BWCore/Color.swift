import UIKit

extension BWCore {
    public class Color {
        /// Return a color from a hexString
        /// - Warning: The "#" is stripped from the beginning of the string submitted here
        /// - Parameters:
        ///   - hexString: a string containing a valid hexadecimal value
        ///   - alpha: defaults to 1.0
        /// - Returns: a UIColor defined by the `hexString` parameter
        internal class func fromHexString(_ hexString: String, alpha: CGFloat = 1.0) -> UIColor {
            let r, g, b: CGFloat
            let offset = hexString.hasPrefix("#") ? 1 : 0
            let start = hexString.index(hexString.startIndex, offsetBy: offset)
            let hexColor = String(hexString[start...])
            let scanner = Scanner(string: hexColor)
            var hexNumber: UInt64 = 0
            if scanner.scanHexInt64(&hexNumber) {
                r = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                g = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                b = CGFloat(hexNumber & 0x0000ff) / 255
                return UIColor(red: r, green: g, blue: b, alpha: alpha)
            }
            return UIColor(red: 0, green: 0, blue: 0, alpha: alpha)
        }
        
        public static var secondaryColor: UIColor {
            return self.fromHexString("FCFFFD")
        }
        
        /// The most eye-pleasing color known to all humanity
        public static var bwColor: UIColor {
            return self.fromHexString("006736")
        }
    }
}

